const fs = require('fs')
const os = require('os')

const nameToGreet = getInput('who-to-greet')
const greetStr = `Hello ${nameToGreet}!`
console.log(`printEnvAction main envs:`)
for (const key of Object.keys(process.env)) {
    console.log(`${key} = ${process.env[key]}`)
}
setOutput('greet-string', greetStr)

function getInput(name, options) {
    const val = process.env[`INPUT_${name.replace(/ /g, '_').toUpperCase()}`] || ''
    if (options && options.required && !val) {
        return ""
    }
  
    if (options && options.trimWhitespace === false) {
        return val
    }
  
    return val.trim()
}

function setOutput(name, value) {
    const filePath = process.env['GITHUB_OUTPUT'] || ''
    if (!filePath) {
        return
    }
    if (!fs.existsSync(filePath)) {
        return
    }
    
    fs.appendFileSync(filePath, `${name}=${value}${os.EOL}`, {
        encoding: 'utf8'
    })
}
  